import { OK, BAD_REQUEST } from 'http-status-codes'
import { Controller, Get } from '@overnightjs/core'
import { Request, Response } from 'express'
import querystring from 'querystring'
import axios from 'axios'
import SpotifyUsers from '../entities/SpotifyUsers'
import { Spotify } from '../interfaces/spotify'
import jwt from 'jsonwebtoken'
const JWT_SECRET_KEY = '&l&2EN[J>t&3|DnipzO.+M{,Nu3:})'

const client_id = '0e8cf78877ec4644a855b24f7c6104cb' // Your client id
const client_secret = '8faea287974244978f961ce49e5939e3' // Your secret
const redirect_uri = 'http://localhost:8080/login' // Your redirect uri
const scope = 'user-read-private user-read-email'

@Controller('spotify')
export default class SpotifyController {
  @Get('login')
  private async getLogin(req: Request, response: Response) {
    const state = generateRandomString(16)
    const query = querystring.stringify({
      response_type: 'code',
      client_id,
      scope: scope,
      redirect_uri,
      state,
    })
    return response.status(OK).json({
      login_url: `https://accounts.spotify.com/authorize?${query}`,
    })
  }

  @Get('finish_login')
  private async finishLogin(request: Request, response: Response) {
    try {
      const code: string = request.query.code
      const Authorization = `Basic ${Buffer.from(
        `${client_id}:${client_secret}`
      ).toString('base64')}`
      const data = querystring.stringify({
        grant_type: 'authorization_code',
        code,
        redirect_uri,
      })

      const spotifyTokenResponse = await axios({
        url: 'https://accounts.spotify.com/api/token',
        method: 'post',
        headers: {
          Authorization,
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        data,
      })
      const authorization: Spotify.Authorization = spotifyTokenResponse.data

      const spotifyUser = await SpotifyUsers.getUserFromSpotifyApi(
        authorization.access_token
      )

      let user = await SpotifyUsers.findOne({
        where: {
          spotify_user_id: spotifyUser.id,
        },
      })
      if (user) {
        await user.refreshUserTokens(authorization)
      } else {
        user = new SpotifyUsers({
          spotify_user_id: spotifyUser.id,
          access_token: authorization.access_token,
          refresh_token: authorization.refresh_token,
        })
        await user.save()
      }

      const podcasts_api_token = jwt.sign(
        {
          user_id: user.id,
        },
        JWT_SECRET_KEY,
        {
          expiresIn: '1 minute',
        }
      )

      return response
        .status(OK)
        .json({
          spotifyUser,
          podcasts_api_token,
          spotify_access_token: authorization.access_token,
        })
    } catch (error) {
      console.error(error)
      return response.status(BAD_REQUEST).json({ message: 'error' })
    }
  }
}

const generateRandomString = function (length: number) {
  let text = ''
  const possible =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'

  for (let i = 0; i < length; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length))
  }
  return text
}
