import {BaseEntity, Entity, PrimaryGeneratedColumn, Column, BeforeUpdate} from "typeorm";
import axios from 'axios'
import { Spotify } from '../interfaces/spotify'

@Entity({ name: 'spotify_users' })
export default class SpotifyUsers extends BaseEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column({ unique: true })
  spotify_user_id: string;

  @Column({ select: false })
  access_token: string

  @Column({ select: false })
  refresh_token: string
  
  @Column('timestamptz', {
    default: 'now'
  })
  created_at: Date

  @Column('timestamptz', {
    default: 'now'
  })
  updated_at: Date

  @BeforeUpdate()
  onBeforeUpdate() {
    this.updated_at = new Date()
  }

  constructor(user: Partial<SpotifyUsers>) {
    super()
    Object.assign(this, user)
  }

  static async getUserFromSpotifyApi(livingAccessToken: string) {
    const response = await axios({
      url: 'https://api.spotify.com/v1/me/',
      headers: {
        Authorization: `Bearer ${livingAccessToken}`
      }
    })
    const spotifyUser: Spotify.User = response.data
    return spotifyUser
  }

  async refreshUserTokens(authorization: Spotify.Authorization) {
    this.access_token = authorization.access_token
    if (authorization.refresh_token) {
      this.refresh_token = authorization.refresh_token
    }
    return this.save()
  }
}