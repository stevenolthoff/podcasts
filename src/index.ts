import * as bodyParser from 'body-parser';
import { Server } from '@overnightjs/core';
import cookieParser from 'cookie-parser'
import { createConnection, Connection } from 'typeorm'
import ITunesController from './routes/itunes'
import SpotifyController from './routes/spotify'

export class PodcastServer extends Server {
    
    constructor() {
        super(process.env.NODE_ENV === 'development'); // setting showLogs to true
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({extended: true}));
        this.app.use(cookieParser())
        this.setupCors()
        this.setupControllers();
    }

    private setupCors(): void {
      this.app.use((request, response, next) => {
        response.header('Access-Control-Allow-Origin', '*') // FIXME
        response.header('Access-Control-Allow-Headers', '*') // FIXME
        if (request.method === 'OPTION') {
          response.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET')
          return response.status(200).json({})
        }
        next()
      })
    }
 
    private setupControllers(): void {
      const iTunesController = new ITunesController()
      const spotifyController = new SpotifyController
      super.addControllers([
        iTunesController,
        spotifyController
      ])
      console.log('Setup controllers.')
    }

    public async setupDatabase(): Promise<void> {
      const connection: Connection = await createConnection()
      // await connection.runMigrations()
      console.log('Setup database connections.')
    }
 
    public start(port: number): void {
        this.app.listen(port, () => {
            console.log('Server listening on port: ' + port);
        })
    }
}

try {
  const podcastServer = new PodcastServer()
  podcastServer.setupDatabase().then(() => {
    const port = process.env.PORT ? parseInt(process.env.PORT) : 3000
    podcastServer.start(port)
  })
} catch (error) {
  console.error(error)
  console.error('Failed to start server.')
}
