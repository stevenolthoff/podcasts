import { OK, BAD_REQUEST } from 'http-status-codes'
import { Controller, Middleware, Get, Post, Put, Delete, } from '@overnightjs/core';
import { Request, Response, } from 'express'
import axios from 'axios'
import convert from 'xml-js'

interface PodcastResponse {
  resultCount: number,
  results: any[],
  jsonFeed: any
}

@Controller('itunes')
export default class ITunesController {
  @Get('podcasts/:id')
  private async getPodcast(req: Request, response: Response) {
    try {
      let podcastResponse: PodcastResponse = {
        resultCount: 0,
        results: [],
        jsonFeed: {}
      }

      const iTunesResponse = await axios({
        method: 'get',
        url: 'https://itunes.apple.com/lookup',
        params: {
          id: req.params.id
        }
      })
      podcastResponse.resultCount = iTunesResponse.data.resultCount
      podcastResponse.results = iTunesResponse.data.results

      const podcastFound = iTunesResponse.data.results.length > 0
      if (podcastFound) {
        const podcast = iTunesResponse.data.results[0]
        const feedResponse = await axios({
          method: 'get',
          url: podcast.feedUrl
        })
        const xmlFeed = feedResponse.data
        const jsonFeed: string = convert.xml2json(xmlFeed, { compact: true, spaces: 4 })
        podcastResponse.jsonFeed = JSON.parse(jsonFeed)
      }
      return response.status(OK).json(podcastResponse)
    } catch (error) {
      console.error(error)
      return response.status(BAD_REQUEST)
    }
  }
}
