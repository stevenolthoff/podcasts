
# Requirements

- I can search for podcasts.
- I can subscribe to podcasts.
- I can see my subscriptions.
- I can bookmark timestamps on podcast episodes.
- I can see my bookmarks.
- I can favorite episodes.
- I can see my favorites.


# Flows

## Open dashboard

- Login
- Get all subscriptions for user
- Front-end hits iTunes API. For each subscription, get album info.

## Search and Subscribe

- User enters search query
- Get results directly from iTunes
- User subscribes to a podcast
- Save this subscription in database
- If iTunes id exists in podcasts table,